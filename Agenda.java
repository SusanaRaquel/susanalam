import java.util.Scanner;
import Contato.Contato;

public class Agenda{
    public Contato[] contatos = new Contato[100];

    public static void main(String[] args){
        Agenda obj = new Agenda();
        Scanner scan = new Scanner(System.in);
        obj.zerar();

        int a = 0;
        String nome;
        while(a != 9){

            System.out.println("\nBem vindo a agenda:\n1-Adicionar contato\n2-Buscar contato\n3-Excluir Contato\n9-Excluir\n");
            a = scan.nextInt();
            switch(a){
                case 1:
                    System.out.print("Digite o nome: ");
                    nome = scan.next();
                    System.out.print("\nDigite o e-mail: ");
                    String email = scan.next();
                    obj.inserir(nome,email);
                    break;
                case 2:
                    System.out.print("Digite o nome a ser buscado: ");
                    nome = scan.next();
                    Contato contatobuscado = obj.busca(nome);
                    if(contatobuscado != null){
                        contatobuscado.imprimir();
                    }
                    break;
                case 3:
                    System.out.print("Digite o nome a ser excluído: ");
                    nome = scan.next();
                    obj.remover(nome);
                    break;
                default:
                    System.out.println("Opção inválida!");
                    break;
            }
        }
        System.out.println("Tchau!");
    }

    public void zerar(){
        int i = 0;
        while(i < 100){
            this.contatos[i] = null;
            i++;
        }
    }

    public Contato busca(String nome){
        int i = 0;
        while(i < 100){
            if(this.contatos[i].retornar_nome() == nome){
                return this.contatos[i];
            }
            i++;
        }
        System.out.println("Nome não encontrado.");
        return null;
    }

    public void inserir(String nome,String email){
        int i = 0;
        while(i < 100){
            if(this.contatos[i] == null){
                this.contatos[i].adicionar(nome,email);
                System.out.println("Contato adicionado.");
                break;
            }
            i++;
        }
        System.out.println("Não foi possível adicionar um contato: Lista cheia!!");
    }

    public void remover(String nome){
        int i = 0;
        while(i < 100){
            if(this.contatos[i].retornar_nome() == nome){
                this.contatos[i] = null;
              System.out.println("Contato excluído.");
              break;
            }
            i++;
        }
        System.out.println("Nome não encontrado.");
    }
}
